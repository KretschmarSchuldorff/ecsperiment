﻿using System;
using SadConsole;
using SadConsole.Controls;
using Console = SadConsole.Console;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using ECSperiments.Defines.Entities;
//using GoRogue;

namespace ECSperiments.Managers
{
    
    class UIManager : ContainerConsole
    {
        public static ScrollingConsole MapConsole;

        private Button startGameButton = new Button(18)
        {
            UseMouse = true,
            IsVisible = true,
            IsFocused = true,
            Position = new Point(1, 1),
            Text = "Start"
        };
        private Button quitGameButton = new Button(18)
        {
            UseMouse = true,
            IsVisible = true,
            IsFocused = false,
            Position = new Point(1, 8),
            Text = "Quit"
        };

        private static Window mainMenu = new Window(20, 10)
        {
            UseMouse = true,
            FocusOnMouseClick = false,
            IsVisible = true,
            IsFocused = false,
            Position = new Point(2, 2)
        };
        private static Window charGen = new Window(80, 25)
        {
            UseMouse = true,
            IsVisible = true,
            Position = new Point(2, 2)
        };

        public UIManager()
        {
            IsVisible = true;
            IsFocused = true;
            Parent = SadConsole.Global.CurrentScreen;
            MapConsole = new ScrollingConsole(100, 100, Global.FontDefault);

        }

        public void MainMenu()
        {
            
            mainMenu.Add(startGameButton);
            mainMenu.Add(quitGameButton);

            SadConsole.Global.CurrentScreen = mainMenu;
            SadConsole.Global.FocusedConsoles.Set(mainMenu);
            startGameButton.Click += EStartGame;
            quitGameButton.Click += EQuitGame;
        }
        public override void Update(TimeSpan timeElapsed)
        {
            Debug.WriteLine("in UI update");
            CheckKeyboard();
            base.Update(timeElapsed);
        }
        public void CenterOnActor(GameEntity ent)
        {
            MapConsole.CenterViewPortOnPoint(ent.Position);
        }

        private static void HideMainMenu()
        {
            mainMenu.Hide();
        }

        void EStartGame(object sender, EventArgs e)
        {
            //GenerateCharacter();
            SwitchToGameView();
        }

        private void SwitchToGameView()
        {

            MapConsole = new ScrollingConsole(ECSperiment.MapWidth, ECSperiment.MapHeight, Global.FontDefault, new Rectangle(0, 0, 50, 20), ECSperiment.GameWorld.CurrentMap.ConvertMap());
            MapConsole.UseKeyboard = false;
            
            Window MapWindow = new Window(80, 25);
            MapWindow.Children.Add(MapConsole);
            Children.Add(MapWindow);
            for (int i = 0; i < ECSperiment.GameWorld.CurrentMap.TerrainArray.Length; i++)
            {
                if (ECSperiment.GameWorld.CurrentMap.TerrainArray[i].IsWalkable)
                {
                    // Set the player's position to the index of the current map position
                    ECSperiment.GameWorld.Player.Position = SadConsole.Helpers.GetPointFromIndex(i, ECSperiment.GameWorld.CurrentMap.Width);
                    break;
                }
            }
            HideMainMenu();
            Debug.WriteLine("player entity on layer: {0}", ECSperiment.GameWorld.Player.Layer);
            SadConsole.Global.CurrentScreen = MapWindow;
            MapConsole.Children.Add(ECSperiment.GameWorld.Player);
            CenterOnActor(ECSperiment.GameWorld.Player);
            SadConsole.Global.FocusedConsoles.Set(MapConsole);
            MapWindow.Show();
        }
        private void GenerateCharacter()
        {

        }

        private static void EQuitGame(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        //public bool MoveActorBy(GameEntity ent, Point position)
        //{
        //    // store the actor's last move state
        //    _lastMoveActor = actor;
        //    _lastMoveActorPoint = position;
        //    return actor.MoveBy(position);
        //}
        public void CheckKeyboard()
        {
            // Keyboard movement for Player character: Up arrow
            // Decrement player's Y coordinate by 1
            if (SadConsole.Global.KeyboardState.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                MoveActorBy(ECSperiment.GameWorld.Player, new Point(0, -1));
                CenterOnActor(ECSperiment.GameWorld.Player);
            }

            // Keyboard movement for Player character: Down arrow
            // Increment player's Y coordinate by 1
            if (SadConsole.Global.KeyboardState.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                MoveActorBy(ECSperiment.GameWorld.Player, new Point(0, 1));
                CenterOnActor(ECSperiment.GameWorld.Player);
            }

            // Keyboard movement for Player character: Left arrow
            // Decrement player's X coordinate by 1
            if (SadConsole.Global.KeyboardState.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Left))
            {
                MoveActorBy(ECSperiment.GameWorld.Player, new Point(-1, 0));
                CenterOnActor(ECSperiment.GameWorld.Player);
            }

            // Keyboard movement for Player character: Right arrow
            // Increment player's X coordinate by 1
            if (SadConsole.Global.KeyboardState.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Right))
            {
                MoveActorBy(ECSperiment.GameWorld.Player, new Point(1, 0));
                CenterOnActor(ECSperiment.GameWorld.Player);
            }
            // Undo last command: Z
            //if (SadConsole.Global.KeyboardState.IsKeyReleased(Microsoft.Xna.Framework.Input.Keys.Z))
            //{
            //    ECSperiment.CommandManager.UndoMoveActorBy();
            //    CenterOnActor(ECSperiment.GameWorld.Player);
            //}

            // Repeat last command: X
            //if (SadConsole.Global.KeyboardState.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.X))
            //{
            //    ECSperiment.CommandManager.RedoMoveActorBy();
            //    CenterOnActor(ECSperiment.GameWorld.Player);
            //}


            if (SadConsole.Global.KeyboardState.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                Environment.Exit(0);
            }
        }

        private bool MoveActorBy(GameEntity player, Point positionChange)
        {
            if (ECSperiment.GameWorld.CurrentMap.GetTerrain(player.Position + new GoRogue.Coord(positionChange.X, positionChange.Y)).IsWalkable)
                return player.MoveBy(positionChange);
            else
                return false;
        }
    }
}
