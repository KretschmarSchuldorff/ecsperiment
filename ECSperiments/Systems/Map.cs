﻿using System;
using System.Diagnostics;
using ECSperiments.Defines.World;
using GoRogue;
using GoRogue.MapGeneration;
using GoRogue.MapViews;
using Microsoft.Xna.Framework;


namespace ECSperiments.Systems
{
    public class Map : GoRogue.GameFramework.Map//, IGameObject
    {
        private ArrayMap<bool> _terrainMap;
        private int _width;
        private int _height;
        private Distance _distance;
        private Random pickFloor = new Random();
        public Map(int width, int height, int layers, Distance distance = null) : base(width, height, layers, distance)
        {
            if (distance == null)
            {
                distance = Distance.EUCLIDEAN;
            }
            else
            {
                _distance = distance;
            }
            _width = width;
            _height = height;
            TerrainArray = new Terrain[_width * _height];
            _terrainMap = new ArrayMap<bool>(_width, _height);
        }

        internal Terrain[] TerrainArray { get; private set; }
        public Distance Distance { get => _distance; set => _distance = value; }

        public void GenerateMap()
        {
            
            int tile;
            QuickGenerators.GenerateCellularAutomataMap(_terrainMap);
            foreach (var pos in _terrainMap.Positions())
            {
                tile = pickFloor.Next(0, 2);
                if (_terrainMap[pos]) // Floor
                {
                    if (tile == 1)
                        this.SetTerrain(new Terrain(pos, Color.LawnGreen, Color.Green, '.', true, true, "Floor"));
                    else
                        this.SetTerrain(new Terrain(pos, Color.DarkGreen, Color.Green, ',', true, true, "Floor"));
                }
                else if (_terrainMap[pos] == false)
                {
                    if (this != null)
                        this.SetTerrain(new Terrain(pos, Color.LightBlue, Color.LightSkyBlue, '~', false, true, "Water"));
                }
                else
                {
                    Debug.WriteLine("Potential null?");
                }
            }
            //Debug.WriteLine("Terrain is {0}", this.Terrain);            
        }
         
        public Terrain[] ConvertMap()
        {
            Terrain[] _cArray = new Terrain[Width * Height];
            foreach (Coord pos in this.Positions())
            {
                _cArray[Coord.ToIndex(pos.X, pos.Y, _width)] = (Terrain)GetTerrain(pos);
                //Debug.WriteLine("IGameObject ID: {0}",_cArray[Coord.ToIndex(pos.X, pos.Y, _width)].ID );
            }
            TerrainArray = _cArray;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(_cArray);
            return _cArray;
        }
    }
}
