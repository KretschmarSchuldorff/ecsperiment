﻿using ECSperiments.Defines.Components;
using ECSperiments.Defines.Entities;
using Microsoft.Xna.Framework;

namespace ECSperiments.Systems
{   
    internal class World
    {
        private GameEntity PlayerEntity;
        public static GoRogue.IDGenerator IDGenerator = new GoRogue.IDGenerator(); // A static IDGenerator that all Entities can access
        public World(int width, int height)
        {
            CurrentMap = new Map(width, height, 1, GoRogue.Distance.MANHATTAN);
            CurrentMap.GenerateMap();
            //Debug.WriteLine("Current Map Terrain: {0}", CurrentMap.Terrain);
            
            PlayerEntity = InitPlayer();
        }
        public GameEntity InitPlayer()
        {
            GameEntity _p = new GameEntity(new GoRogue.Coord(0,0), 1, Color.DarkBlue, Color.Transparent, 2);
            _p.AddComponent(new ComponentMovable());
            _p.AddComponent(new ComponentHealth(100f));
            //Debug.WriteLine("HP component {0}", _p.GetComponent<ComponentHealth>().Hp);
            return _p;
        }
        public Map CurrentMap { get; set; }
        public GameEntity Player { get => PlayerEntity; set => PlayerEntity = value; }
    }
}
