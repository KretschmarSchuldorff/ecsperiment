﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECSperiments.ECS;
using SadConsole;
using SadConsole.Controls;

namespace ECSperiments.ECS
{
    interface IGenericComponent
    {
        void ECSEventHandler<CustomEventArgs>(object sender, CustomEventArgs e);
    }
}
