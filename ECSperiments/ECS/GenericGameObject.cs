﻿using System.Diagnostics;

namespace ECSperiments.ECS
{
    class GenericGameObject : IGenericComponent
    {
        private readonly string id;
        public void ECSEventHandler<CustomEventArgs>(object system, CustomEventArgs e)
        {
            Debug.WriteLine("Shit happened: " + e);
        }
        public GenericGameObject(string ID, GenericSystem pub)
        {
            id = ID;
            // Subscribe to the event using C# 2.0 syntax
            pub.RaiseCustomEvent += ECSEventHandler;
        }
        void HandleCustomEvent(object sender, CustomEventArgs e)
        {
            Debug.WriteLine(id + " received this message: {0}", e.Message);
        }
    }
}
