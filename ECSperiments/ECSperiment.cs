﻿using System;
using SadConsole;
using Console = SadConsole.Console;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SadConsole.Components;
using ECSperiments.ECS;
using ECSperiments.Managers;
using ECSperiments.Systems;
using System.Diagnostics;

namespace ECSperiments
{
    /// <summary>
    /// The main class.
    /// </summary>
    public class ECSperiment
    {

        //public System.IO.DirectoryInfo[] DataFiles;
        static UIManager uiManager;
        internal static World GameWorld;
        internal static GoRogue.IDGenerator IDGenerator = new GoRogue.IDGenerator(); // A static IDGenerator that all Entities can access

        public static int MapWidth { get; internal set; }
        public static int MapHeight { get; internal set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // Setup the engine and creat the main window.
            SadConsole.Game.Create(80, 25);

            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Hook the update event that happens each frame so we can trap keys and respond.
            SadConsole.Game.OnUpdate = Update;
            ((SadConsole.Game)SadConsole.Game.Instance).WindowResized += Program_WindowResized;
            
            // Start the game.
            SadConsole.Game.Instance.Run();
            

            //Debug.WriteLine("Screen width: {0}, Screen height: {1}", SadConsole.Global.WindowWidth, SadConsole.Global.WindowHeight);

            //
            // Code here will not run until the game window closes.
            //

            SadConsole.Game.Instance.Dispose();
        }
        private static void Update(GameTime time)
        {
            uiManager.CheckKeyboard();
            //Debug.WriteLine("in MainLoop update");
        }

        private static void Program_WindowResized(object sender, EventArgs e)
        {
            uiManager.Clear();
            SadConsole.Global.CurrentScreen.Resize(SadConsole.Global.WindowWidth / SadConsole.Global.CurrentScreen.Font.Size.X,
                SadConsole.Global.WindowHeight / SadConsole.Global.CurrentScreen.Font.Size.Y, false);
        }

        static void Init()
        {
            SadConsole.Settings.AllowWindowResize = true;
            SadConsole.Settings.ResizeMode = SadConsole.Settings.WindowResizeOptions.None;
            MapWidth = 1000;
            MapHeight = 1000;
            uiManager = new UIManager();
            GameWorld = new World(MapWidth, MapHeight);
            Debug.WriteLine(Environment.CurrentDirectory);
            var DataFiles = System.IO.Directory.EnumerateFiles(Environment.CurrentDirectory, "data/components/*.json");
            foreach(var file in DataFiles)
            {
                Debug.WriteLine($"found file {file}");
                var loadedComponent = Newtonsoft.Json.JsonConvert.DeserializeObject<Defines.Components.ComponentVanillaHumanoid>(System.IO.File.ReadAllText(file));
                Debug.WriteLine($"{loadedComponent.BaseAttack}d10");
                Debug.WriteLine(System.IO.Path.GetFileNameWithoutExtension(file));
                System.Json.JsonValue jsonValue = System.Json.JsonValue.Parse(System.IO.File.ReadAllText(file));
                Debug.WriteLine($"{jsonValue["BaseHP"]}");
            }
            //DataFiles.GetD

            uiManager.MainMenu();
        }

    }
}
