﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ECSperiments.Defines.Components
{
    internal class ComponentVanillaHumanoid : IGameObjectComponent
    {
        public IGameObject Parent { get; set; }
        public double BaseHP { get; set; }
        public double CurrentHP { get; set; }
        public int BaseWill { get; set; }
        public int CurrentWill { get; set; }
        public int Actions { get; set; }
        public string BaseAttack { get; set; }
        public string BaseDefense { get; set; }
        public int Cost { get; set; }
        public ComponentVanillaHumanoid()
        {
            //BaseHP = 10;
            //CurrentHP = BaseHP;
            //BaseWill = 10;
            //CurrentWill = BaseWill;
            //Actions = 2;
            //BaseAttack = "1d10";
            //BaseDefense = "1d10";
            //Cost = 0;
        }
    }
}
