﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ECSperiments.Defines.Components
{
    public class ComponentMovable : IGameObjectComponent
    {
        public IGameObject Parent { get; set; }
        public ComponentMovable()
        {
        }
    }
}
