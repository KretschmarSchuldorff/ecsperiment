﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ECSperiments.Defines.Components
{
    internal class ComponentHealth : IGameObjectComponent
    {
        public IGameObject Parent { get; set; }
        public string ComponentName { get; }
        public float Hp { get; }

        public ComponentHealth(float hp, string componentName = "Hitpoints" )
        {
            Hp = hp;
            ComponentName = componentName;
        }
    }
}
