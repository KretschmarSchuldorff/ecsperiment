﻿using GoRogue.GameFramework;
using SadConsole.Entities;
using GoRogue;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using SadConsole.Components;

namespace ECSperiments.Defines.Entities
{
    internal class GameEntity : Entity, IGameObject
    {
        private GameObject _entityBacking;
        
        Point pos = new Point();
        public GameEntity(Coord position,
                          int layer,
                          Color foreground,
                          Color background,
                          int glyph,
                          int width = 1,
                          int height = 1,
                          bool isWalkable = true,
                          bool isTransparent = true) : base(width, height)
        {
            pos.X = position.X;
            pos.Y = position.Y;
            Animation.CurrentFrame[0].Foreground = foreground;
            Animation.CurrentFrame[0].Background = background;
            Animation.CurrentFrame[0].Glyph = glyph;
            _entityBacking = new GameObject(position, layer, parentObject: this, isStatic: true, isWalkable: isWalkable, isTransparent: isTransparent);

            // Ensure that the entity position/offset is tracked by scrollingconsoles
            Components.Add(new EntityViewSyncComponent());
        }
        public bool MoveBy(Point positionChange)
        {
            Position += positionChange;
            return true;
        }

        public Point Pos { get => pos; set => pos = value; }

        public Map CurrentMap => ((IGameObject)_entityBacking).CurrentMap;

        public bool IsStatic => ((IGameObject)_entityBacking).IsStatic;

        public bool IsTransparent { get => ((IGameObject)_entityBacking).IsTransparent; set => ((IGameObject)_entityBacking).IsTransparent = value; }
        public bool IsWalkable { get => ((IGameObject)_entityBacking).IsWalkable; set => ((IGameObject)_entityBacking).IsWalkable = value; }

        public uint ID => ((IGameObject)_entityBacking).ID;

        public int Layer => ((IGameObject)_entityBacking).Layer;

        Coord IGameObject.Position { get => ((IGameObject)_entityBacking).Position; set => ((IGameObject)_entityBacking).Position = value; }

        event EventHandler<ItemMovedEventArgs<IGameObject>> IGameObject.Moved
        {
            add
            {
                ((IGameObject)_entityBacking).Moved += value;
            }

            remove
            {
                ((IGameObject)_entityBacking).Moved -= value;
            }
        }

        public void AddComponent(object component)
        {
            ((IGameObject)_entityBacking).AddComponent(component);
        }

        public T GetComponent<T>()
        {
            return ((IGameObject)_entityBacking).GetComponent<T>();
        }

        public IEnumerable<T> GetComponents<T>()
        {
            return ((IGameObject)_entityBacking).GetComponents<T>();
        }

        public bool HasComponent(Type componentType)
        {
            return ((IGameObject)_entityBacking).HasComponent(componentType);
        }

        public bool HasComponent<T>()
        {
            return ((IGameObject)_entityBacking).HasComponent<T>();
        }

        public bool HasComponents(params Type[] componentTypes)
        {
            return ((IGameObject)_entityBacking).HasComponents(componentTypes);
        }

        public bool MoveIn(Direction direction)
        {
            return ((IGameObject)_entityBacking).MoveIn(direction);
        }

        public void OnMapChanged(Map newMap)
        {
            ((IGameObject)_entityBacking).OnMapChanged(newMap);
        }

        public void RemoveComponent(object component)
        {
            ((IGameObject)_entityBacking).RemoveComponent(component);
        }

        public void RemoveComponents(params object[] components)
        {
            ((IGameObject)_entityBacking).RemoveComponents(components);
        }
    }
}
