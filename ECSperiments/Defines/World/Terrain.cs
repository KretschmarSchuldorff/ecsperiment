﻿using System;
using System.Collections.Generic;
using GoRogue;
using GoRogue.GameFramework;
using Microsoft.Xna.Framework;
using SadConsole;

namespace ECSperiments.Defines.World
{
    public class Terrain : Cell, IGameObject
    {
        private GameObject _terrainBacking;
        private bool _isWalkable;
        private bool _isTransparent;
        private string _name;
        
        public Terrain(Coord position,
                       Color foreground,
                       Color background,
                       int glyph,
                       bool isWalkable,
                       bool isTransparent,
                       string name) : base(foreground, background, glyph)
        {
            _terrainBacking = new GameObject(position, layer: 0, parentObject: this, isStatic: true, isWalkable: isWalkable, isTransparent: isTransparent);
            _isWalkable = isWalkable;
            _isTransparent = isTransparent;
            _name = name;
        }
        Point Coord2Point(Coord position)
        {
            int x = position.X;
            int y = position.Y;
            return new Point(x,y);
        }
        //public bool IsWalkable { get => _isWalkable; set => _isWalkable = value; }
        //public bool IsTransparent { get => _isTransparent; set => _isTransparent = value; }
        public string Name { get => _name; set => _name = value; }

        #region autoimplemented IGameObject

        public Map CurrentMap => ((IGameObject)_terrainBacking).CurrentMap;

        public bool IsStatic => ((IGameObject)_terrainBacking).IsStatic;

        public bool IsTransparent { get => ((IGameObject)_terrainBacking).IsTransparent; set => ((IGameObject)_terrainBacking).IsTransparent = value; }
        public bool IsWalkable { get => ((IGameObject)_terrainBacking).IsWalkable; set => ((IGameObject)_terrainBacking).IsWalkable = value; }
        public Coord Position { get => ((IGameObject)_terrainBacking).Position; set => ((IGameObject)_terrainBacking).Position = value; }

        public uint ID => ((IGameObject)_terrainBacking).ID;

        public int Layer => ((IGameObject)_terrainBacking).Layer;

        public event EventHandler<ItemMovedEventArgs<IGameObject>> Moved
        {
            add
            {
                ((IGameObject)_terrainBacking).Moved += value;
            }

            remove
            {
                ((IGameObject)_terrainBacking).Moved -= value;
            }
        }

        public void AddComponent(object component)
        {
            ((IGameObject)_terrainBacking).AddComponent(component);
        }

        public T GetComponent<T>()
        {
            return ((IGameObject)_terrainBacking).GetComponent<T>();
        }

        public IEnumerable<T> GetComponents<T>()
        {
            return ((IGameObject)_terrainBacking).GetComponents<T>();
        }

        public bool HasComponent(Type componentType)
        {
            return ((IGameObject)_terrainBacking).HasComponent(componentType);
        }

        public bool HasComponent<T>()
        {
            return ((IGameObject)_terrainBacking).HasComponent<T>();
        }

        public bool HasComponents(params Type[] componentTypes)
        {
            return ((IGameObject)_terrainBacking).HasComponents(componentTypes);
        }

        public bool MoveIn(Direction direction)
        {
            return ((IGameObject)_terrainBacking).MoveIn(direction);
        }

        public void OnMapChanged(Map newMap)
        {
            ((IGameObject)_terrainBacking).OnMapChanged(newMap);
        }

        public void RemoveComponent(object component)
        {
            ((IGameObject)_terrainBacking).RemoveComponent(component);
        }

        public void RemoveComponents(params object[] components)
        {
            ((IGameObject)_terrainBacking).RemoveComponents(components);
        }
        #endregion
    }
}
